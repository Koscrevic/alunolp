object Form3: TForm3
  Left = 0
  Top = 0
  Caption = 'Form3'
  ClientHeight = 196
  ClientWidth = 275
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 47
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Label2: TLabel
    Left = 8
    Top = 93
    Width = 46
    Height = 13
    Caption = 'Treinador'
  end
  object Label3: TLabel
    Left = 8
    Top = 133
    Width = 23
    Height = 13
    Caption = 'Nivel'
  end
  object DBEdit1: TDBEdit
    Left = 8
    Top = 66
    Width = 121
    Height = 21
    DataField = 'nome'
    DataSource = DataModule2.DataSource2
    TabOrder = 0
  end
  object DBEdit3: TDBEdit
    Left = 8
    Top = 152
    Width = 121
    Height = 21
    DataField = 'nivel'
    DataSource = DataModule2.DataSource2
    TabOrder = 1
  end
  object Button1: TButton
    Left = 168
    Top = 66
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Cancelar: TButton
    Left = 168
    Top = 108
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = CancelarClick
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 8
    Top = 112
    Width = 121
    Height = 21
    DataField = 'id_treinador'
    DataSource = DataModule2.DataSource2
    KeyField = 'id_treinador'
    ListField = 'nome'
    ListSource = DataModule2.DataSource1
    TabOrder = 4
  end
end
